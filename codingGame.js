//Adding text on the beginning of the key 

var arr = { "un" : 1, "deux" : 2, "trois": 3 };  

for (const [key, value] of Object.entries(arr)) {
  console.log(` toto-${key}: ${value}`);
}


//Sort an array with keys values

//Example 1
var arr = [
    {value: 21 },
    {value: 37 },
    {value: 45 },
    {value: -12 },
    {value: 13 },
    {value: 37 }
  ];
  arr.sort(function (a, b) {
    return b.value - a.value;
  });
  
console.log(arr);

//Example 2
var arr = [{ "key" : 4 }, { "key" : 90 }, { "key" : 3 }, { "key" : 84 }, { "key" : 96 }, { "key" : 12 }, { "key" : 6 }, { "key" : 1 }] ; 
arr.sort(function (a, b) {
    return b.key - a.key;
}); 
console.log(arr);


//Example 3
var arr = { "toto" : 4, "tata" : 90, "titi" : 3, "coco" : 84, "wiwi" : 96, "micka" : 12, "mimi" : 6, "nini" : 1}; 

function getSortedKeys(arr) {
 
  var keys = []; 
  var values = [];
  for(const [key, value] of Object.entries(arr)){
    keys.push(key);
    values.push(value);
    
  } 
  values.sort((a, b) => b - a);
  keys.sort(function(a,b){ return arr[b]-arr[a];})
  console.log(values); 
  console.log(keys);


  return keys.sort(function(a,b){return arr[b]-arr[a]});
  
}

getSortedKeys(arr);



//Parse string 
var str = "Hello world!";
var hello = str.substr(0, 5);
var world = str.substr(6, 5);

console.log(hello + '\n' + world);


//Index of a value
const arr = ['coco', 'toto', 'titi', 'tata', 'tutu'];
function modulu(arr){
  if(arr.indexOf('titi') % 2 === 0){
      return true;
  }
  
  return false;
}

console.log(arr.indexOf('titi') % 2 === 0);


// Select all the values situated on pair index

const arr = ['coco', 'toto', 'titi', 'tata', 'tutu'];
var tab = [];


function modulo(arr){
  for(var i = 0; i <= arr.length; i++)
  {
    
    if(arr.indexOf(arr[i]) % 2 === 0){
        console.log(arr[i]);
        return arr[i];
    }

  }
  
  return arr[i];
}
modulo(arr);







