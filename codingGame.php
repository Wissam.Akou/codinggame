<?php

// return max
function findLargest(array $numbers) {
    $size = sizeof($numbers);
    $max = $numbers[0];
    for ($i = 0; $i < $size - 1; $i++)
        if ($numbers[$i] < $numbers[$i + 1] && $numbers[$i + 1] > $max)
            $max = $numbers[$i + 1];

    
    if($max == max($numbers))
        return $max;

}

//key value
$array = array(
    'fruit1' => 'apple',
    'fruit2' => 'orange',
    'fruit3' => 'grape',
    'fruit4' => 'apple',
    'fruit5' => 'apple');

// Cette boucle affiche toutes les clés
// dont la valeur vaut "apple"

function fruits(array $array){
    
    while ($fruit_name = current($array)) {
        if ($fruit_name == 'apple') {
            echo key($array), "\n";
        }
        next($array);
    }

}

fruits($array);


// sum d'une variable 

$input = '9,3,5,5,7,8';
echo array_sum(explode(',', $input));

//Parse string 
var str = "Hello world!";
var hello = str.substr(0, 5);
var world = str.substr(6, 5);

//Adding text on the beginning of the key 
$array = array('a', 'b', 'c');
array_walk($array, function(&$value, $key) { $value = 'coucou-'. $value; } );
var_dump($array);

// sort array décroissant
$weight = [
    'Pete' => 75, 
    'Benjamin' => 62,
    'Jonathan' => 101
  ];  	
arsort($weight);
var_dump($weight);

// sort array croissant
$weight = [
    'Pete' => 75, 
    'Benjamin' => 62,
    'Jonathan' => 101
  ];  	
asort($weight);
var_dump($weight);


//sort array jey value 
$inventory = array(
    array("price"=>5.43),
    array("price"=>3.50),
    array("price"=>2.90),
 );

$price = array();
foreach ($inventory as $key => $row)
{
    $price[$key] = $row['price'];
}
array_multisort($price, SORT_DESC, $inventory); // array_multisort($price, SORT_DESC, $inventory); croissant
var_dump($price);

// an other way to sort 

$inventory = [
    "price"=>5.43,
    "price"=>3.50,
    "price"=>2.90,
 ];

$price = [];
foreach ($inventory as $key )
{
    $price[$key] = $key;
}
array_multisort($price, SORT_ASC, $inventory);

var_dump ($price);


// sort array 

function cmp($a, $b)
{
    if ($a == $b) {
        return 0;
    }
    return ($a < $b) ? -1 : 1;
}

$a = array(3, 2, 5, 6, 1);

usort($a, "cmp");

foreach ($a as $key => $value) {
    echo "$key: $value\n";
}



?>


